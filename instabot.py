import os
import time
import MySQLdb
import sys
from tempfile import gettempdir

from selenium.common.exceptions import NoSuchElementException

from instapy import InstaPy

user_id = str(sys.argv[1])

# set headless_browser=True if you want to run InstaPy on a server

# set these in instapy/settings.py if you're locating the
# library in the /usr/lib/pythonX.X/ directory:
#   Settings.database_location = '/path/to/instapy.db'
#   Settings.chromedriver_location = '/path/to/chromedriver'

# Open database connection
db = MySQLdb.connect("localhost","root","toor","laravel" )

# prepare a cursor object using cursor() method
cursor = db.cursor()
queryParameters = ("SELECT * FROM parameters WHERE user_id = %s")
queryModes = ("SELECT * FROM bot_using_logs WHERE user_id = %s AND process_id = (SELECT process_id from bot_using_logs WHERE user_id = %s ORDER BY process_id DESC LIMIT 1)")

# execute SQL query using execute() method.
cursor.execute(queryParameters, (user_id))
# Fetch a single row using fetchone() method.
resultsParameters = cursor.fetchall()
cursor.execute(queryModes, (user_id,user_id))
resultsBotProcess = cursor.fetchall()
#id | instagram_username | instagram_password | min_followers | max_followers |
# min_following | max_following | potency_ratio | delimit_by_numbers | created_at          | updated_at

for row in resultsParameters:
  id = row[1]
  insta_username = row[2]
  insta_password = row[3]
  rb_min_followers = row[4]
  rb_max_followers = row[5]
  rb_min_following = row[6]
  rb_max_following = row[7]
  rb_potency_ratio = row[8]
  rb_delimit_by_numbers = row[9]

try:
  session = InstaPy(username=insta_username, password=insta_password,
                    headless_browser=True, bypass_suspicious_attempt=False,
                    use_firefox=False,multi_logs=True)
  
  session.login()
  session.set_do_follow(enabled=True, percentage=55, times=2)
  session.set_relationship_bounds(enabled=True,
                               potency_ratio=-rb_potency_ratio,
                                delimit_by_numbers=rb_delimit_by_numbers,
                                 max_followers=rb_max_followers,
                                  max_following=rb_max_following,
                                   min_followers=rb_min_followers,
                                    min_following=rb_min_following)
  for row in resultsBotProcess:
    botCategory = row[3]
    set_id = row[4]
    list_id = row[5]
    
    print "Bot Category:",botCategory
    print "SeT ID",set_id
    if botCategory == 1:
      queryParameterLikeByTags = ("SELECT * FROM parameter_like_by_tags WHERE user_id = %s AND set_id = %s")
      cursor.execute(queryParameterLikeByTags, (user_id, set_id))
      resultsParameterLikeByTags = cursor.fetchall()
      for row in resultsParameterLikeByTags:
        lbt_amount = row[3]
        lbt_randomize = row[4]
        lbt_use_smart_hashtags = 0
        print "Fuehre Like Bot aus fuer (amount=",lbt_amount,", randomize",lbt_randomize,", use_smart_hashtags=",lbt_use_smart_hashtags
        queryLikeByTags = ("SELECT tag_name FROM like_by_tags WHERE user_id = %s AND list_id = %s")
        cursor.execute(queryLikeByTags, (user_id, list_id))
        resultsLikeByTags = cursor.fetchall()
        likes = [];
        for row in resultsLikeByTags:
            likes.append(row[0]);
        print "Listeneintraege ",likes
        if lbt_use_smart_hashtags == 1:
          session.set_smart_hashtags(likes, limit=3, sort='top', log_tags=True)
          session.like_by_tags( amount=lbt_amount, use_smart_hashtags=lbt_use_smart_hashtags, media='Photo')
        else:
          session.like_by_tags( likes, amount=lbt_amount, use_smart_hashtags=lbt_use_smart_hashtags, media='Photo')




    if botCategory == 2:
      queryParameterFollowuserFollowers = ("SELECT * FROM parameter_follow_user_followers WHERE user_id = %s AND set_id = %s")
      cursor.execute(queryParameterFollowuserFollowers, (user_id, set_id))
      resultsParameterFollowUserFollowers = cursor.fetchall()
      for row in resultsParameterFollowUserFollowers:
        fuf_amount = row[3]
        fuf_randomize = row[4]
        fuf_interact = row[5]
        print "Fuehre Follow Bot aus fuer (amount=",fuf_amount,", randomize",fuf_randomize,", interact=",fuf_interact
        queryFollowuserFollowers = ("SELECT follow_name FROM follow_user_followers WHERE user_id = %s AND list_id = %s")
        cursor.execute(queryFollowuserFollowers, (user_id, list_id))
        resultsFollowUserFollowers = cursor.fetchall()
        followUserFollowers = [];
        for row in resultsFollowUserFollowers:
            followUserFollowers.append(row[0]);
        print "Listeneintraege ",followUserFollowers

        session.follow_user_followers(followUserFollowers, amount=fuf_amount, randomize=fuf_randomize, interact=fuf_interact)

    if botCategory == 3:
      queryInteractWithUser = ("SELECT * FROM parameter_interact_with_users WHERE user_id = %s AND set_id = %s")
      cursor.execute(queryInteractWithUser, (user_id, set_id))
      resultsParameterInteractWithUser = cursor.fetchall()
      for row in resultsParameterInteractWithUser:
        ui_amount = row[3]
        ui_randomize = row[4]
        ui_percentage = row[5]
        ui_media = row[6]
        dofollow_enabled = row[7]
        dofollow_percentage = row[8]
        dolike_enabled = row[9]
        dolike_percentage = row[10]
        docomment_enabled = row[11]
        docomment_percentage = row[12]

        session.set_user_interact(amount=ui_amount, randomize=ui_randomize, percentage=ui_percentage, media='Photo')
        session.set_do_follow(enabled=dofollow_enabled, percentage=dofollow_percentage)
        session.set_do_like(enabled=dolike_enabled, percentage=dolike_percentage)
        
        print "Fuehre Interact Bot aus fuer (amount=",ui_amount,", randomize",ui_randomize,", % =",ui_percentage," ui_media=",ui_media," dofollow_enabled = ",dofollow_enabled," dofollow_percentage = ",dofollow_percentage," dolike_enabled = ", dolike_enabled," dolike_percentage = ",dolike_percentage," docomment_enabled = ", docomment_enabled," docomment_percentage = ",docomment_percentage
        queryComments = ("SELECT comment FROM comments WHERE user_id = %s AND list_id = %s")
        cursor.execute(queryComments, (user_id, list_id))
        resultsComments = cursor.fetchall()
        comments = [];
        for row in resultsComments:
            comments.append(row[0]);

        print "Listeneintraege ",comments
        session.set_comments(comments)
        session.set_do_comment(enabled=docomment_enabled, percentage=docomment_percentage)
        session.interact_user_following(['natgeo'], amount=10, randomize=True)
         # disconnect from server
  db.close()



   
     # if UiFollowUserFollowers:
    #    session.set_user_interact(amount=fuf_ui_amount, randomize=fuf_ui_randomize, percentage=fuf_ui_percentage, media='Photo')
    #  session.follow_user_followers(followUserFollowers, amount=fuf_amount, randomize=fuf_randomize, interact=fuf_interact)
        
   # if LikeByTags:
    #  if UiLikeByTags:
    #    session.set_user_interact(amount=lbt_ui_amount, randomize=lbt_ui_randomize, percentage=lbt_ui_percentage, media='Photo')
    #  session.like_by_tags(likes, amount=lbt_amount, use_smart_hashtags=lbt_use_smart_hashtags, media='Photo')
finally:
    # end the bot session
    session.end()
