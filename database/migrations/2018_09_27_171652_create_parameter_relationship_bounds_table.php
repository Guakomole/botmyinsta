<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterRelationshipBoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_relationship_bounds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('set_id');
            $table->integer('min_followers')->default(1);
            $table->integer('max_followers')->default(1);
            $table->integer('min_following')->default(1);
            $table->integer('max_following')->default(1);
            $table->double('potency_ratio')->default(1.3);
            $table->boolean('delimit_by_numbers')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_relationship_bounds');
    }
}
