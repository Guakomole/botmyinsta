<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteractWithUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interact_with_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('ui_amount');
            $table->boolean('ui_randomize');
            $table->integer('ui_percentage');
            $table->integer('ui_media');

            $table->boolean('dofollow_enabled');
            $table->integer('dofollow_percentage');

            $table->boolean('dolike_enabled');
            $table->integer('dolike_percentage');

            $table->boolean('docomment_enabled');
            $table->integer('docomment_percentage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interact_with_users');
    }
}
