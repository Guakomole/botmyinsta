<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     *   rb => relationship_bounds
      *  ui => user_interact
     * @return void
     */
    public function up()
    {
        Schema::create('parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');

            //User data
            $table->string('instagram_username')->default('');
            $table->string('instagram_password')->default('');

            //Relationship Bounds
            $table->integer('rb_min_followers')->default(1);
            $table->integer('rb_max_followers')->default(1);
            $table->integer('rb_min_following')->default(1);
            $table->integer('rb_max_following')->default(1);
            $table->double('rb_potency_ratio')->default(1.3);
            $table->boolean('rb_delimit_by_numbers')->default(0);
        

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter');
    }
}
