<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterLikeByTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_like_by_tags', function (Blueprint $table) {
            $table->increments('id');
            //Like By Tag incl. User Interface
            $table->integer('user_id');
            $table->integer('set_id');
            $table->integer('amount')->default(1);
            $table->boolean('randomize')->default(1);
            $table->boolean('interact')->default(1);
            $table->integer('percentage')->default(1);
            $table->integer('media')->default(1);
            $table->boolean('use_smart_hashtags')->default(1);
            $table->integer('ui_amount')->default(1);
            $table->boolean('ui_randomize')->default(1);
            $table->integer('ui_percentage')->default(1);
            $table->integer('ui_media')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_like_by_tags');
    }
}
