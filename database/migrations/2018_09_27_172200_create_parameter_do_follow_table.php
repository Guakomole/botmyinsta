<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterDoFollowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_do_follow', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('set_id');
            $table->boolean('enabled')->default(0);
            $table->integer('percentage')->default(0);
            $table->integer('times')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_do_follow');
    }
}
