<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterFollowUserFollowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_follow_user_followers', function (Blueprint $table) {
            $table->increments('id');
            //Follow User Followers
            $table->integer('user_id');
            $table->integer('set_id');
            $table->integer('amount')->default(1);
            $table->boolean('randomize')->default(1);
            $table->boolean('interact')->default(1);
            $table->integer('sleep_delay')->default(600);
            $table->integer('ui_amount')->default(1);
            $table->boolean('ui_randomize')->default(1);
            $table->integer('ui_percentage')->default(1);
            $table->integer('ui_media')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_follow_user_followers');
    }
}
