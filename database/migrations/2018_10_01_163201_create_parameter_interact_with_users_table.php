<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterInteractWithUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_interact_with_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('set_id');
            $table->integer('user_id');
            $table->integer('ui_amount')->default(1);
            $table->boolean('ui_randomize')->default(0);
            $table->integer('ui_percentage')->default(1);
            $table->integer('ui_media')->default(1);

            $table->boolean('dofollow_enabled')->default(0);
            $table->integer('dofollow_percentage')->default(1);

            $table->boolean('dolike_enabled')->default(0);
            $table->integer('dolike_percentage')->default(1);

            $table->boolean('docomment_enabled')->default(0);
            $table->integer('docomment_percentage')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_interact_with_users');
    }
}
