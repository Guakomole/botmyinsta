@extends('layouts.app')
@section('content')

     <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Instagram Service Pricing</h1>
      <p class="lead">These are our basic prices. The differences between them are listed <a href="#">here</a>. You can test our service for Free!</p>
    </div>

    <div class="container">
      <div class="card-deck mb-3 text-center">
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Free</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$0 <small class="text-muted">/ Month</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>1 Instagram Account</li>
              <li>1 Keyword</li>
              <li>Low Priority Queue</li>
              <li>No Email support</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-outline-primary">Sign up for free</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Basic</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$10<small class="text-muted">/ Month</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>1 Instagram Account</li>
              <li>10 Keywords</li>
              <li>Normal Priority Queue</li>
              <li>Slow Email support</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Get started</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Pro</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$25<small class="text-muted">/ Month</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>3 Instagram Accounts</li>
              <li>15 Keywords</li>
              <li>Normal Priority Queue</li>
              <li>Slow Email support</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Get started</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Enterprise</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$150 <small class="text-muted">/ Month</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Unlimited Instagram Accounts</li>
              <li>100 Keywords per Account</li>
              <li>High Priority Queue</li>
              <li>Fast Email support</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Contact us</button>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
    <h2>Comparison between Plans</h2>
    <h2>Referral Program</h2>
    </div>

@endsection