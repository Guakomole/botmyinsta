@extends('layouts.app')

@section('content')
<div class="container">
	<h3>Botliste</h3>
	<div class="row">
    	<table class="table">
		  <tr>
		    <th>Bot ID</th>
		    <th>Ui Amount</th> 
		    <th>Ui Randomize</th>
		    <th>Ui Percentage</th>
		    <th>Dof_enabled</th> 
		    <th>Dof_percentage</th>
		    <th>Dol_enabled</th> 
		    <th>Dol_percentage</th>
		    <th>Doc_enabled</th> 
		    <th>Doc_percentage</th>
		    <th>Dont want it anymore ?</th>
		  </tr>
		 @foreach($parameter_interact_with_users as $parameter_interact_with_user)
		 <tr>
		 	<td>{{ $parameter_interact_with_user->set_id}}</td>
		 	<td>{{ $parameter_interact_with_user->ui_amount}}</td>
		 	<td>{{ $parameter_interact_with_user->ui_randomize}}</td>
		 	<td>{{ $parameter_interact_with_user->ui_percentage}}</td>
		 	<td>{{ $parameter_interact_with_user->dofollow_enabled}}</td>
		 	<td>{{ $parameter_interact_with_user->dofollow_percentage}}</td>
		 	<td>{{ $parameter_interact_with_user->dolike_enabled}}</td>
		 	<td>{{ $parameter_interact_with_user->dolike_percentage}}</td>
		 	<td>{{ $parameter_interact_with_user->docomment_enabled}}</td>
		 	<td>{{ $parameter_interact_with_user->docomment_percentage}}</td>
		 	<td><a href="{{ route('deleteInteractWithUserSettings', ['set_id' => $parameter_interact_with_user->set_id]) }}">delete</a></td>
		 </tr>
		 @endforeach
		</table>
	</div>
	<hr>
	<div class="row">
			<div class="col-md-12" id="box">
	            <div class="row">
	            	<div class="col-md-4">
	            		<h4>Liste auswählen</h4>
	            		<form action="{{ route('saveComment') }}" method="post">
							<select name="lists" class="form-control">
							  	@foreach($interactWithUserLists as $interactWithUserList)
							    	 <option value="{{ $interactWithUserList->list_id }}">{{ $interactWithUserList->list_name }} </option>
							    @endforeach
							</select>
							<br>

							<h4>Neuer Listeneintrag</h4>
								<label>Enter new Keyword</label>
					            <input class="form-control" type="text" name="comment" id="comment">
			                <input type="hidden" name="_token" value="{{ Session::token() }}">
		                </form>
	            			
						<br>
						<h4>Neue Liste</h4>
	            		<form action="{{ route('saveCommentList') }}" method="post" enctype="multipart/form-data">
			        		<label>Listenname</label>
			        		<input name="list_name" type="text" class="form-control" id="list_name" placeholder="Enter new Listname">
							<input type="hidden" name="_token" value="{{ Session::token() }}">
						</form>	
					
			        </div>
					<div class="col-md-8">
						<h4>Bishereige Listen</h4>
			             <table class="table">
							  <tr>
							    <th>Listenname</th> 
							    <th>Items</th> 
							  </tr>
							 @foreach($listContainers as $listContainer)
							 <tr>
							 	<td><a href="{{ route('deleteCommentList', ['list_id' => $listContainer->list_id]) }}">{{$listContainer->listname }}</a></td>
							 	<td>
							 	@foreach($listContainer->listItems as $listItem)
							 	   <a href="{{ route('deleteComment', ['list_id' => $listContainer->list_id, 'comment' => $listItem ]) }}">{{$listItem }}</a> ,

							 	@endforeach
							 	</td>
							 </tr>
							 @endforeach
						</table>
	            	</div>
	        </div> 
    	</div>
    </div>
	<hr>
	<h3>Bot hinzufügen</h3>

	<div class="row">
    	<form action="{{ route('saveInteractWithUserSettings') }}" method="post" enctype="multipart/form-data">
	    		<div class="col-md-5">
			        <label for="amount">Amount</label> <input name="ui_amount" type="text" class="form-control" id="ui_amount" placeholder="">
			        <label for="ui_percentage">ui_percentage</label> <input name="ui_percentage" type="text" class="form-control" id="ui_percentage" placeholder="">
			        <label for="dofollow_percentage">dofollow_percentage</label> <input name="dofollow_percentage" type="text" class="form-control" id="dofollow_percentage" placeholder="">
					<label for="dolike_percentage">dolike_percentage</label> <input name="dolike_percentage" type="text" class="form-control" id="dolike_percentage" placeholder="">
					<label for="docomment_percentage">docomment_percentage</label> <input name="docomment_percentage" type="text" class="form-control" id="docomment_percentage" placeholder="">

				</div>
				<div class="col-md-7" id="box">
				
				<input type="radio" id="ui_randomize" name="ui_randomize" value="1" >
				<input type="radio" id="ui_randomize" name="ui_randomize" value="0" checked>
				<label>Random On/Off:</label>
				<br>
				
				<input type="radio" id="dofollow_enabled" name="dofollow_enabled" value="1">
				<input type="radio" id="dofollow_enabled" name="dofollow_enabled" value="0"checked>
				<label>dofollow_enabled On/Off:</label>
				<br>
				<input type="radio" id="dolike_enabled" name="dolike_enabled" value="1">
				<input type="radio" id="dolike_enabled" name="dolike_enabled" value="0"checked>
				<label>dolike_enabled On/Off:</label>
				<br>
				<input type="radio" id="docomment_enabled" name="docomment_enabled" value="1">
				<input type="radio" id="docomment_enabled" name="docomment_enabled" value="0"checked>
				<label>docomment_enabled On/Off:</label>
				<br>

				<button type="submit" id="setting_buttons" class="btn btn-primary" id="submitSettings">Hinzufügen</button>
			    <input type="hidden" name="_token" value="{{ Session::token() }}">
			
			</form>	
		</div>			
	</div>	
</div>
@endsection