@extends('layouts.app')

@section('content')

<div class="position-relative overflow-hidden p-3 p-md-5 bg-light">
	<div class="col-md-5 mx-auto my-5 zindex">
		<h1 class="display-4 font-weight-bold">Do you want to Reach your Audience?</h1>
		<h2 class="lead font-weight-normal">Get followers on Instagram via Bot</h2>
		<p class="font-weight-light">Interact with real people on intergram. Boost your Stats &amp; Sales. Automatically Like, Follow, Comment, Unfollow, DM, and Post in an Instant on Instagram!</p>
		<div class="text-center "> 
			<a class="btn btn-outline-danger w-25 mt-3 center-block" href="/features">Our Features</a>
		</div>
	</div>
	<div class="product-device shadow-sm d-none d-md-block"></div>
	<div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
</div>

<div class="col-md-5 mx-auto my-5">
	<h1 class="no-margin">Instagram Bot</h1>
	<h2 class="lead font-weight-normal ">What's that? Why do I need it?</h2>
	<p class="font-weight-light">Instagram is a big platform. It won't go away soon, so we often better use it! We can't resist technical progress.
		This Bot will help you as an <strong>Influencer or Company</strong> to sustain your growth.
		Maybe you use Instagram already on a daily basis and want to get more time for other things in your life. You
		could be an Instagram Account Manager in a big Agency and want to safe time or make your workflow more efficient. Either way this application
		was made for you!
	</p>
	<p class="font-weight-light">
		Our IG Bot can like, comment and subscribe for you. That means you don't have to do it anymore. Our Bot is Smart. You can change the 
		algorithm in many ways. Instagram Bots are often traceable due to simple coding. We recreate real human interaction on Instagram to
		ensure no bans. If you want to change Settings for more likes or faster growth, we give you this option. Customice your Bot as you like!
	</p>
</div>

<div class="col-md-5 mx-auto my-5">
	<h1 class="no-margin">Bots for Influencers</h1>
	<h2 class="lead font-weight-normal ">Less work, more Followers</h2>
	<p class="font-weight-light">
		You love what you do, but social media takes up to much free time? We're here for you. We know the hassle connected
		to building a community around your work. If you want to grow your Instagram channel on a regualr basis, you have to
		interact with many other users. This takes so much time! Safe it with our Bot. Our IG Bot likes, comments and follows
	Users you should interact with. </p>

	<p class="font-weight-light">
		How do we know, which users you should interact with? Its simple: You tell us. When you setup your account, we will
		personalyze your Bot. These custom Settings can be changes afterwards, but will be the default till then. If you think
		your Bot reaches out to the wrong people, you can tell it our system and the bot will change its behavior. With our free plan,
		you can test our Service as long as you like. It will save you much time and its free - try it now!
	</p>
</div>

<div class="col-md-5 mx-auto my-5">
	<h1 class="no-margin">Real Community Growth</h1>
	<h2 class="lead font-weight-normal ">Why are Bots better than normal Advertising?</h2>
	<p class="font-weight-light">
		A regular User dislikes Ads on the internet. Everyone knows they're useful to keep the social platform running,
		but no one likes them. With regular Advertising on Instagram you also dont interact with your Customer on a
		interessed basis. More than 50% of Instagram Users try to block or skip Ads - thats why you should reach out
		to them with bots! On some occasions you should make use of both: Advertisement and Bots, but in many cases
		are Companys only using bots more efficient and less expensive!
	</p>
	<p class="font-weight-light">
		If you promote you Channel via interaction with other users you will not only get more followers, but will have
		a more immersive connection to your customer. Many young people interact differently today with brands than 20 years ago.
		We don't only buy a thing - we buy an emotion, an idear or a way of living. Thats why many customers try to buy from "good"
		Companys. Be that Company. Show them how awesome you are with our Bot!
	</p>
</div>

@endsection

<style>

.no-margin {
	margin: 0;
}

/*
 * Dummy devices (replace them with your own or something else entirely!)
 */

 .m-md-3 {margin:0 !important;}
 .pt-4, .py-4 {padding-top: 0 !important;}
 .product-device {
 	position: absolute;
 	right: 10%;
 	bottom: -30%;
 	width: 300px;
 	height: 540px;
 	background-color: #333;
 	border-radius: 21px;
 	-webkit-transform: rotate(30deg);
 	transform: rotate(30deg);
 }

 .product-device::before {
 	position: absolute;
 	top: 10%;
 	right: 10px;
 	bottom: 10%;
 	left: 10px;
 	content: "";
 	background-color: rgba(255, 255, 255, .1);
 	border-radius: 5px;
 }

 .product-device-2 {
 	top: -25%;
 	right: auto;
 	bottom: 0;
 	left: 5%;
 	background-color: #e5e5e5;
 }


/*
 * Extra utilities
 */

 .flex-equal > * {
 	-ms-flex: 1;
 	flex: 1;
 }
 @media (min-width: 768px) {
 	.flex-md-equal > * {
 		-ms-flex: 1;
 		flex: 1;
 	}
 }
 .overflow-hidden { overflow: hidden;}
 .zindex { z-index: 999; }
</style>