@extends('layouts.app')

@section('content')
<div class="container">
  <?php /* 
    <form action="{{ route('runBot') }}" method="post" enctype="multipart/form-data">
    <div class="row">
          <div class="col-md-6">
            <h2>Like by Tags</h2><br>
             <table class="table">

              <thead class="thead-dark">
              <tr>
                <th>Activate</th>
                <th>amount</th> 
                <th>random</th> 
                <th>inter</th> 
                <th>%</th> 
                <th>media</th> 
                <th>sh</th> 
                <th>Liste</th> 
              </tr>
                </thead>
              <?php $x = 0 ?>
             @foreach($parameter_like_by_tags as $parameter_like_by_tag)
             <tr>
              <td>
                  <input type="hidden" name="lbt_bot_<?php echo $x ?>" value="0"> 
                  <input id="bot_checkbox" type="checkbox" name="lbt_bot_<?php echo $x ?>" value="{{ $parameter_like_by_tag->set_id}}">
              </td>
              <td>{{ $parameter_like_by_tag->amount}}</td>
              <td>{{ $parameter_like_by_tag->randomize}}</td>
              <td>{{ $parameter_like_by_tag->interact}}</td>
              <td>{{ $parameter_like_by_tag->percentage}}</td>
              <td>{{ $parameter_like_by_tag->media}}</td>
              <td>{{ $parameter_like_by_tag->use_smart_hashtags}}</td>
              <td>
                <select name="lists_lbt_<?php echo $x ?>" class="form-control">
                    @foreach($likeLists as $likeList)
                       <option value="{{ $likeList->list_id }}">{{ $likeList->list_name }} </option>
                    @endforeach
                </select>
             </td>
             </tr>
              <?php $x = $x+1 ?>
             @endforeach
            </table>
          </div>
    <hr>
          <div class="col-md-6">
            <h2>Follow User Followers</h2><br>
             <table class="table">
              <thead class="thead-dark">
              <tr>
                <th>Activate</th>
                <th>amount</th> 
                <th>random</th> 
                <th>inter</th> 
                <th>Sleep Delay</th> 
                <th>Liste</th> 
              </tr>
              </thead>
              <?php $x = 0 ?>
             @foreach($parameter_follow_user_followers as $parameter_follow_user_follower)
             <tr>
              <td>
                  <input type="hidden" name="fuf_bot_<?php echo $x ?>" value="0"> 
                  <input id="bot_checkbox" type="checkbox" name="fuf_bot_<?php echo $x ?>" value="{{ $parameter_follow_user_follower->set_id}}">
              </td>
              <td>{{ $parameter_follow_user_follower->amount}}</td>
              <td>{{ $parameter_follow_user_follower->randomize}}</td>
              <td>{{ $parameter_follow_user_follower->interact}}</td>
              <td>{{ $parameter_follow_user_follower->sleep_delay}}</td>
              <td>
                <select name="lists_fuf_<?php echo $x ?>" class="form-control">
                    @foreach($followLists as $followList)
                       <option value="{{ $followList->list_id }}">{{ $followList->list_name }} </option>
                    @endforeach
                </select>
             </td>
             </tr>
             <?php $x = $x+1 ?>
             @endforeach
            </table>
          </div>
     </div>
    <hr>
      <div class="row">
         <div class="col-md-12">
        <h2>Interact With Users</h2><br>
      <table class="table">
        <thead class="thead-dark" id="botThead">
        <tr>
          <th>Activate</th>
          <th>Ui Amount</th> 
          <th>Ui Randomize</th>
          <th>Ui %</th>
          <th>Dof_enabled</th> 
          <th>Dof %</th>
          <th>Dol_enabled</th> 
          <th>Dol %</th>
          <th>Doc_enabled</th> 
          <th>Doc %</th>
          <th>Dont want it anymore ?</th>
        </tr>
        </thead>
        <?php $x = 0 ?>
       @foreach($parameter_interact_with_users as $parameter_interact_with_user)
       <tr>
        <td>
            <input type="hidden" name="ui_bot_<?php echo $x ?>" value="0"> 
            <input id="bot_checkbox" type="checkbox" name="ui_bot_<?php echo $x ?>" value="{{ $parameter_interact_with_user->set_id}}">
        </td>
        <td>{{ $parameter_interact_with_user->ui_amount}}</td>
        <td>{{ $parameter_interact_with_user->ui_randomize}}</td>
        <td>{{ $parameter_interact_with_user->ui_percentage}}</td>
        <td>{{ $parameter_interact_with_user->dofollow_enabled}}</td>
        <td>{{ $parameter_interact_with_user->dofollow_percentage}}</td>
        <td>{{ $parameter_interact_with_user->dolike_enabled}}</td>
        <td>{{ $parameter_interact_with_user->dolike_percentage}}</td>
        <td>{{ $parameter_interact_with_user->docomment_enabled}}</td>
        <td>{{ $parameter_interact_with_user->docomment_percentage}}</td>
        <td>
            <select name="lists_ui_<?php echo $x ?>" class="form-control">
                @foreach($commentLists as $commentList)
                   <option value="{{ $commentList->list_id }}">{{ $commentList->list_name }} </option>
                @endforeach
            </select>
         </td>
      </tr>
      <?php $x = $x+1 ?>
       @endforeach
      </table>
    </div>
    </div>
  <hr>
    <button type="submit" id="submitRunbot" class="btn btn-primary">Bot jetzt starten</button>
    <input type="hidden" name="_token" value="{{ Session::token() }}">
</form>
</div>
*/ ?> 


<style>

main {
    background-color: #f8fafc;
}




.btn.btn-primary, button {
    border-width: 1px;
    border-radius:4px;
    color: #fff;

    background-color: #d81616;
    border-color: #d81616;

    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
}

.btn.btn-primary:hover, button:hover {
    background-color: #343a40;
    border-color: #343a40;
    color: #fff;
    
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
  }

  /** ADD PROCESS / BOT [LIGHTBOX] **/

  input.newbot.textbox {
    width: 100%;
    border-radius: 5px;
    border-width: 1px;
    border-color:#ffffff;
    background-color: #ececec;
    height:30px;
    
    transition: all .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
  }

input.newbot.textbox:focus {
    background-color: #ffffff;
    border-color:#d81616;
    outline:0;
}

input.newbot.textbox {
    margin: 5px 0px 5px 0px;
}

button.addprocess {   
    margin: 20px 0px 10px 0px;
}

  /** EOF ADD PROCESS / BOT **/

  form#filterbots {
    margin: 0 auto;
    margin-bottom: 30px;
}

.filterbuttons {
    text-align: center;
    margin-top: 10px;
}

/** STYLING BOT BOXES **/

.botarea {
    width: 100%;
}

.botcontainer {
    display: inline-block;
    width: auto;
    margin-right: 10px;
    margin-bottom: 15px;
    color: white;
    padding: 10px;
}

.bot.active {
    background-color:#d81616;
}

.bot.done {
    background-color: #18b20e;
}

.bot {
    background-color:#343a40;
    padding: 5px;
}

.botvalues {
    background-color: #ececec;
    padding: 10px;
    color: #343a40;
}

.startbotbuttons {
    margin-top: 15px;
}

.startbotbuttons button {
    border-width: 0;
    border-radius: 0px;
    color: #fff;
    background-color: #a4a4a4;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
}

.startbotbuttons button:hover {
    border-width: 0;
    border-radius: 0px;
    color: #fff;
    background-color: #797979;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
}
/** EOF BOTS **/

 .triangle-right { 
  width: 0;
height: 0;
border-top: 5px solid transparent;
border-left: 10px solid #fff;
border-bottom: 5px solid transparent;
display: inline-block;
 }

.button.pause {
width: 0px;
height: 10px;
border-color: #fff;
border-style: double;
border-width: 0px 0px 0px 8px;
display: inline-block;
top: 1px;
position: relative;
}

}</style>

<div class = "modal fade" id="masuk" role="dialog">
  <div class = "modal-dialog">
    <div class = "modal-content">
      <div class = "modal-header">
        <h4 class="modal-title">Add Process</h4>
      </div>
      <div class = "modal-body">
          <form action="{{ route('saveNewBot') }}" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-6">
              <select name="BotList" class="form-control">
                  <option value="0">Hashtag</option>
                  <option value="1">Kanal</option>
                  <option value="2">Nutzer</option>
              </select>
            </div>
            <div class="col-md-6">
                <input type="textbox" name="newBotKeywordTextbox" placeholder="#Hashtag, Kanalname"></input>
            </div>
            </div>
              <div class="row">
                <div class="col-md-4">

                </div>
                <div class="col-md-6">
                  <input type="hidden" name="newBotUseGlovalSettingsCheckbox" value="0"></input>
                  <input type="checkbox" name="newBotUseGlovalSettingsCheckbox" value="1">Use global settings</input>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" class="newBotParameterContainer">
                <h4>Set maximum Actions</h4>

                  Max. Likes
                  <input type="textbox" class="newbot textbox" placeholder="450"><br>
                  
                  Max. Follows
                  <input type="textbox" class="newbot textbox" placeholder="100"><br>

                  Max. Comments                  
                  <input type="textbox" class="newbot textbox" placeholder="5">
              </div>
              </div>
          <button type="submit" class="btn btn-primary addprocess" id="submitSettings">Bot erstellen</button>
          <input type="hidden" name="_token" value="{{ Session::token() }}">
          <button type="button" class="btn btn-primary m-t-10" data-dismiss="modal">Close</button>
        </form>
      </div>
      <div class="modal-footer">
      </div>
      </div>
      
    </div>
  </div>


  <h1>Your saved Bots / Actions on Insragram</h1>

    <p class="bot active" style="color:white;">Your Bot is active right now!</p>
  <p class="bot" style="color:white;">Your Bot is inactive or on Schedule</p>
  <p class="bot done" style="color:white;">Action has been accomplished!</p>

<div class="startbotbuttons">
  <button>Start <div class="triangle-right"></div></button>
  <button>Pause <div class="button pause"></div></button>
</div>


<div class="row" id="box">
<form action="{{ route('getSelectedBots') }}" method="post" enctype="multipart/form-data" id="filterbots">
  <input type="checkbox" name="getHashtagBots" value="1">Hashtags</input>
  <input type="checkbox" name="getKanalBots" value="1">Kanäle</input>
  <input type="checkbox" name="getNutzerBots" value="1">Nutzer</input>
    <div class="filterbuttons">
      <button type="submit" id="test" class="btn btn-primary" id="submitSettings">Use filter</button>
      <button type="submit" id="test" class="btn btn-primary" id="submitSettings">Show All</button>
    </div>
  <input type="hidden" name="_token" value="{{ Session::token() }}">
</form>
</div>


  <div class="row">

<div class="botarea">
@foreach($configured_user_bots as $configured_user_bot)
<div class="botcontainer">
  @if($configured_user_bot->mode === 0)
<div class="bot active hashtag">
  #{{ $configured_user_bot->keyword }}
@elseif($configured_user_bot->mode === 1)
<div class="bot kanal">
  [Kanal]{{ $configured_user_bot->keyword }}
@elseif($configured_user_bot->mode === 2)
<div class="bot nutzer done">
  [Nutzer]{{ $configured_user_bot->keyword }}
@endif  
</div>


<div class="botvalues">
  <div class="botlikes">Likes: 0/150</div>
  <div class="botfollows">Follows: 1/20</div>
  <div class="botcomments">Comments: 0/0</div>
<div class="startbotbuttons">
<button>Start / Pause</button>
</div>


</div>
</div>
@endforeach
</div>
</div>
<div class="row" id="box">
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#masuk">Bot erstellen</button>
</div>

</div>

@endsection



