@extends('layouts.app')

@section('content')
<div class="container">

	<h3>Botliste</h3>
	<div class="row">
		<table class="table">
		  <tr>
		    <th>Bot ID</th>
		    <th>amount</th> 
		    <th>randomize</th> 
		    <th>interact</th> 
		    <th>Percentage</th> 
		    <th>media</th> 
		    <th>use_sh</th> 
		    <th>ui_amount</th> 
		    <th>ui_randomize</th> 
		    <th>ui_percentage</th> 
		    <th>ui_media</th> 
		    <th>Dont want it anymore ?</th>
		  </tr>
		 @foreach($parameter_like_by_tags as $parameter_like_by_tag)
		 <tr>
		 	<td>{{ $parameter_like_by_tag->set_id}}</td>
		 	<td>{{ $parameter_like_by_tag->amount}}</td>
		 	<td>{{ $parameter_like_by_tag->randomize}}</td>
		 	<td>{{ $parameter_like_by_tag->interact}}</td>
		 	<td>{{ $parameter_like_by_tag->percentage}}</td>
		 	<td>{{ $parameter_like_by_tag->media}}</td>
		 	<td>{{ $parameter_like_by_tag->use_smart_hashtags}}</td>
		 	<td>{{ $parameter_like_by_tag->ui_amount}}</td>
		 	<td>{{ $parameter_like_by_tag->ui_randomize}}</td>
		 	<td>{{ $parameter_like_by_tag->ui_percentage}}</td>
			<td>{{ $parameter_like_by_tag->ui_media}}</td>
		 	<td><a href="{{ route('deleteLikeByTagSettings', ['set_id' => $parameter_like_by_tag->set_id]) }}">delete</a></td>
		 </tr>
		 @endforeach
		</table>
	</div>
	<hr>
	<h3>Like By Tags</h3>

	<div class="row">
		<div class="col-md-12" id="box">
	            <div class="row">
	            	<div class="col-md-4">
	            		<h4>Liste auswählen</h4>
	            		<form action="{{ route('saveLike') }}" method="post">
							<select name="lists" class="form-control">
							  	@foreach($likeLists as $likeList)
							    	 <option value="{{ $likeList->list_id }}">{{ $likeList->list_name }} </option>
							    @endforeach
							</select>
							<br>

							<h4>Neuer Listeneintrag</h4>
								<label>Enter new Keyword</label>
					            <input class="form-control" type="text" name="like" id="like">
			                <input type="hidden" name="_token" value="{{ Session::token() }}">
		                </form>
	            			
						<br>
						<h4>Neue Liste</h4>
	            		<form action="{{ route('saveLikeByTagList') }}" method="post" enctype="multipart/form-data">
			        		<label>Listenname</label>
			        		<input name="list_name" type="text" class="form-control" id="list_name" placeholder="Enter new Listname">
							<input type="hidden" name="_token" value="{{ Session::token() }}">
						</form>	
					
			        </div>
					<div class="col-md-8">
						<h4>Bishereige Listen</h4>
			             <table class="table">
							  <tr>
							    <th>Listenname</th> 
							    <th>Items</th> 
							  </tr>
							 @foreach($listContainers as $listContainer)
							 <tr>
							 	<td><a href="{{ route('deleteLikeByTagList', ['list_id' => $listContainer->list_id]) }}">{{$listContainer->listname }}</a></td>
							 	<td>
							 	@foreach($listContainer->listItems as $listItem)
							 	   <a href="{{ route('deleteLike', ['list_id' => $listContainer->list_id, 'tag_name' => $listItem ]) }}">{{$listItem }}</a> ,

							 	@endforeach
							 	</td>
							 </tr>
							 @endforeach
						</table>
	            	</div>
	        </div> 
    	</div>
    </div>
    <hr>
    <h3>Bot hinzufügen</h3>
    <div class="row">
    	<form action="{{ route('saveLikeByTagSettings') }}" method="post" enctype="multipart/form-data">
	    	<div class="col-md-6" id="box">
		        <label for="amount">Amount</label> <input name="amount" type="text" class="form-control" id="amount" placeholder="">
		        <label for="percentage">percentage</label> <input name="percentage" type="text" class="form-control" id="settings-textbox" placeholder="">

			</div>
			<div class="col-md-5" id="box">
				<label>Random On/Off:</label>
				<input type="radio" id="randomize" name="randomize" value="1" >
				<input type="radio" id="randomize" name="randomize" value="0" checked>
				<br>
				<label>Interact On/Off:</label>
				<input type="radio" id="interact" name="interact" value="1">
				<input type="radio" id="interact" name="interact" value="0"checked>
				<br>
				<label>Use Smart On/Off:</label>
				<input type="radio" id="use_smart_hashtags" name="use_smart_hashtags" value="1">
				<input type="radio" id="use_smart_hashtags" name="use_smart_hashtags" value="0"checked>
				<button type="submit" id="setting_buttons" class="btn btn-primary" id="submitSettings">Hinzufügen</button>
			    <input type="hidden" name="_token" value="{{ Session::token() }}">
			</form>	
		</div>			
	</div>
	<hr>
	<div class="row">
		<form action="{{ route('saveLikeByTagSettings') }}" method="post" enctype="multipart/form-data">
			<div class="col-md-7" id="box">
				<h4>User Interact Options</h4>
					Random On/Off     
					<input type="radio" id="ui_randomize" name="ui_randomize" value="1">
					<input type="radio" id="ui_randomize" name="ui_randomize" value="0"checked>				
					Amount 
					<input name="ui_amount" type="text" class="form-control" id="settings-textbox" placeholder="">
					<br>Percentage 
					<input name="ui_percentage" type="text" class="form-control" id="settings-textbox" placeholder="">
					<button type="submit" id="setting_buttons" class="btn btn-primary" id="submitSettings">Save Data</button>
			   		<input type="hidden" name="_token" value="{{ Session::token() }}">	
				</div>
			</form>	
	</div>
</div>
@endsection