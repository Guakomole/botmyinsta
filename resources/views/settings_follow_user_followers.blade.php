@extends('layouts.app')

@section('content')
<div class="container">
	<h3>Botliste</h3>
	<div class="row">
		<table class="table">
		  <tr>
		    <th>Bot ID</th>
		    <th>Amount</th> 
		    <th>Random</th> 
		    <th>Sleep Delay</th>
		    <th>Dont want it anymore ?</th>
		  </tr>
		 @foreach($parameter_follow_user_followers as $parameter_follow_user_follower)
		 <tr>
		 	<td>{{ $parameter_follow_user_follower->set_id}}</td>
		 	<td>{{ $parameter_follow_user_follower->amount}}</td>
		 	<td>{{ $parameter_follow_user_follower->randomize}}</td>
		 	<td>{{ $parameter_follow_user_follower->sleep_delay}}</td>
		 	<td><a href="{{ route('deleteFollowUserFollowerSettings', ['set_id' => $parameter_follow_user_follower->set_id]) }}">delete</a></td>
		 </tr>
		 @endforeach
		</table>
	</div>
	<hr>
	<h2>Follow User Followers</h2>
	<div class="row">
		<div class="col-md-12" id="box">
	            <div class="row">
	            	<div class="col-md-4">
	            		<h4>Liste auswählen</h4>
	            		<form action="{{ route('saveFollowUserFollowers') }}" method="post">
							<select name="lists" class="form-control">
							  	@foreach($followLists as $followList)
							    	 <option value="{{ $followList->list_id }}">{{ $followList->list_name }} </option>
							    @endforeach
							</select>
							<br>

							<h4>Neuer Listeneintrag</h4>
								<label>Enter new Keyword</label>
					            <input class="form-control" type="text" name="follow" id="follow">
			                <input type="hidden" name="_token" value="{{ Session::token() }}">
		                </form>
	            			
						<br>
						<h4>Neue Liste</h4>
	            		<form action="{{ route('saveFollowUserFollowerList') }}" method="post" enctype="multipart/form-data">
			        		<label>Listenname</label>
			        		<input name="list_name" type="text" class="form-control" id="list_name" placeholder="Enter new Listname">
							<input type="hidden" name="_token" value="{{ Session::token() }}">
						</form>	
					
			        </div>
					<div class="col-md-8">
						<h4>Bishereige Listen</h4>
			             <table class="table">
							  <tr>
							    <th>Listenname</th> 
							    <th>Items</th> 
							  </tr>
							 @foreach($listContainers as $listContainer)
							 <tr>
							 	<td><a href="{{ route('deleteFollowUserFollowerList', ['list_id' => $listContainer->list_id]) }}">{{$listContainer->listname }}</a></td>
							 	<td>
							 	@foreach($listContainer->listItems as $listItem)
							 	   <a href="{{ route('deleteFollowUserFollowers', ['list_id' => $listContainer->list_id, 'follow_name' => $listItem ]) }}">{{$listItem }}</a> ,

							 	@endforeach
							 	</td>
							 </tr>
							 @endforeach
						</table>
	            	</div>
	        </div> 
    	</div>
    </div>
    <hr>
    	<div class="row">
		<form action="{{ route('saveFollowUserFollowerSettings') }}" method="post" enctype="multipart/form-data">
	    	<div class="col-md-8" id="box">
		        <label for="amount">Amount</label>
		        <input name="amount" type="text" class="form-control" id="amount" placeholder="">
				<br>Sleep Delay (sec) 
				<input name="sleep_delay" type="text" class="form-control" id="sleep_delay" placeholder="">
			</div>
	    	<div class="col-md-4" id="box">
				<label>Random On/Off</label>
					<input type="radio" id="randomize" name="randomize" value="1">
					<input type="radio" id="randomize" name="randomize" value="0">
				<button type="submit" id="setting_buttons" class="btn btn-primary" id="submitSettings">Save Data</button>
			    <input type="hidden" name="_token" value="{{ Session::token() }}">
			</form>	
			</div>
	</div>
	<hr>
	<div class="row">
		<form action="{{ route('saveFollowUserFollowerSettings') }}" method="post" enctype="multipart/form-data">
			<div class="col-md-8">
				<h4>User Interact Options</h4>			
					<br>Amount 
					<input name="ui_amount" type="text" class="form-control" id="settings-textbox" placeholder="">
					<br>Percentage 
					<input name="ui_percentage" type="text" class="form-control" id="settings-textbox" placeholder="">
					<button type="submit" id="setting_buttons" class="btn btn-primary" id="submitSettings">Save Data</button>
			   		<input type="hidden" name="_token" value="{{ Session::token() }}">
			</div>
			<div class="col-md-4" id="box">
				Random On/Off     
					<input type="radio" id="ui_randomize" name="ui_randomize" value="1">
					<input type="radio" id="ui_randomize" name="ui_randomize" value="0"checked>
			</div>
		</form>				
	</div>	
</div>
@endsection