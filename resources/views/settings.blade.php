@extends('layouts.app')


@section('content')
<div class="container">
	<h2>Accountdaten</h2>
	<div class="row">
		<form action="{{ route('saveSettings', ['return' => 'settings']) }}" method="post" enctype="multipart/form-data">
			<div class="col-md-4" id="box">
				<h3>Instagram</h3>
				Username <input value="{{ $settings->first()->instagram_username }}" name="instagram_username" type="text" class="form-control" id="settings-textbox" aria-describedby="userName" placeholder="">
				Password <input value="{{ $settings->first()->instagram_password }}" name="instagram_password" type="password" class="form-control" id="settings-textbox" aria-describedby="userName" placeholder="">
				<button type="submit" id="setting_buttons" class="btn btn-primary" id="submitSettings">Save Data</button>
			    <input type="hidden" name="_token" value="{{ Session::token() }}">
			</div>	
		</form>
	</div>
	<hr>
	<h2>Relationships</h2>
	<div class="row">
		<form action="{{ route('saveSettings', ['return' => 'settings']) }}" method="post" enctype="multipart/form-data">
			<div class="col-md-4" id="box">
				min. Followers <input value="{{ $settings->first()->rb_min_followers }}" name="rb_min_followers" type="text" class="form-control" id="settings_minmax_follower" aria-describedby="userName" placeholder="">
				min. following <input value="{{ $settings->first()->rb_min_following }}" name="rb_min_following" type="text" class="form-control" id="settings_minmax_follower" aria-describedby="userName" placeholder="">
			</div>
			
			<div class="col-md-4" id="box">
				max. Followers <input value="{{ $settings->first()->rb_max_followers }}" name="rb_max_followers" type="text" class="form-control" id="settings_minmax_follower" aria-describedby="userName" placeholder="">
				max. Following <input value="{{ $settings->first()->rb_max_following }}" name="rb_max_following" type="text" class="form-control" id="settings_minmax_follower" aria-describedby="userName" placeholder="">
			</div>
			<div class="col-md-4" id="box">
				Potency ratio <input value="{{ $settings->first()->rb_potency_ratio }}" name="rb_potency_ratio" type="text" class="form-control" id="settings_potency_ratio" aria-describedby="userName" placeholder="">
				<br>
				<button type="submit" id="setting_buttons" class="btn btn-primary" id="submitSettings">Save Data</button>
		    	<input type="hidden" name="_token" value="{{ Session::token() }}">
		    </div>
		</form>
	</div>
</div>
@endsection