@extends('layouts.app')

@section('content')
<div class="container">
	<h3>Botliste</h3>
	<div class="row">
		<table class="table">
		  <tr>
		    <th>Bot ID</th>
		    <th>Percentage</th> 
		    <th>Dont want it anymore ?</th>
		  </tr>
		 @foreach($parameter_follow_by_tags as $parameter_follow_by_tag)
		 <tr>
		 	<td>{{ $parameter_follow_by_tag->set_id}}</td>
		 	<td>{{ $parameter_follow_by_tag->amount}}</td>
		 	<td><a href="{{ route('deleteFollowByTagSettings', ['set_id' => $parameter_follow_by_tag->set_id]) }}">delete</a></td>
		 </tr>
		 @endforeach
		</table>
	</div>
	<hr>
	<h3>Followliste</h3>
	<div class="row">
	<div class="col-md-4" id="box">
		<form action="{{ route('saveFollowByTag') }}" method="post">
	        <div class="form-group">
	           <label for="like">Follow Tags</label><br>
	            <input class="form-control" type="text" name="follow_by_tag" id="follow_by_tag">
	            @foreach($follow_by_tags as $follow_by_tag)
	                <a href="{{ route('deleteFollowByTag', ['id' => $follow_by_tag->id]) }}">{{  $follow_by_tag->tag_name }}</a> |
	            @endforeach
	            <input type="hidden" name="_token" value="{{ Session::token() }}">
	        </div>
        </form>
    </div>
    </div>
    <hr>
    <h3>Bot hinzufügen</h3>
	<div class="row">
    	<div class="col-md-4" id="box">
    	<form action="{{ route('saveFollowByTagSettings') }}" method="post" enctype="multipart/form-data">
	        <label for="amount">Amount</label> <input name="amount" type="text" class="form-control" id="amount" placeholder="">
		
			<label>Enabled On/Off:</label>
				<input type="radio" id="com_enabled" name="com_enabled" value="1" checked>
				<input type="radio" id="com_enabled" name="com_enabled" value="0">
		<button type="submit" id="setting_buttons" class="btn btn-primary" id="submitSettings">Hinzufügen</button>
		<input type="hidden" name="_token" value="{{ Session::token() }}">
		</form>		
	</div>	
	</div>
</div>
@endsection