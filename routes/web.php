<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('features', function () {
    return view('features');
});

Route::get('prices', function () {
    return view('prices');
});

Auth::routes();

Route::get('/home', 'HomeController@getDashboard')->name('home');
Route::get('/settings', 'SettingsController@index')->name('settings');
Route::get('/botSettings', 'BotSettingsController@index')->name('botSettings');
Route::get('/settingsFollowUserFollowers', 'HomeController@getSettingsFollowUserFollowers')->name('settingsFollowUserFollowers');
Route::get('/settingsLikeByTags', 'HomeController@getSettingsLikeByTags')->name('settingsLikeByTags');
Route::get('/settingsFollowByTags', 'HomeController@getSettingsFollowByTags')->name('settingsFollowByTags');
Route::get('/settingsCommentTags', 'HomeController@getSettingsCommentTags')->name('settingsCommentTags');
Route::post('/runBot', 'HomeController@runBot')->name('runBot');

//Save Settings
Route::post('/saveNewBot', [
        'uses' => 'CreateNewBotController@saveNewBot',
        'as' => 'saveNewBot',
       
    ]);
Route::post('/getSelectedBots', [
        'uses' => 'CreateNewBotController@getSelectedBots',
        'as' => 'getSelectedBots',
       
    ]);
Route::post('/saveSettings/{return}', [
        'uses' => 'SettingsController@saveSettings',
        'as' => 'saveSettings',
       
    ]);
Route::post('/saveCommentSettings/', [
        'uses' => 'CommentsController@saveCommentSettings',
        'as' => 'saveCommentSettings',
    ]);
Route::get('/deleteCommentSettings/{comment_id}', [
    'uses' => 'CommentsController@deleteCommentSettings',
    'as' => 'deleteCommentSettings',
    'middleware' => 'auth'
]);
Route::post('/saveLikeByTagSettings/', [
        'uses' => 'LikeByTagController@saveLikeByTagSettings',
        'as' => 'saveLikeByTagSettings',
    ]);
Route::get('/deleteLikeByTagSettings/{like_id}', [
    'uses' => 'LikeByTagController@deleteLikeByTagSettings',
    'as' => 'deleteLikeByTagSettings',
    'middleware' => 'auth'
]);
Route::post('/saveFollowByTagSettings/', [
        'uses' => 'FollowByTagController@saveFollowByTagSettings',
        'as' => 'saveFollowByTagSettings',
    ]);
Route::get('/deleteFollowByTagSettings/{follow_id}', [
    'uses' => 'FollowByTagController@deleteFollowByTagSettings',
    'as' => 'deleteFollowByTagSettings',
    'middleware' => 'auth'
]);

Route::post('/saveFollowUserFollowerSettings/', [
        'uses' => 'FollowUserFollowersController@saveFollowUserFollowerSettings',
        'as' => 'saveFollowUserFollowerSettings',
    ]);

Route::get('/deleteFollowUserFollowerSettings/{follow_id}', [
    'uses' => 'FollowUserFollowersController@deleteFollowUserFollowerSettings',
    'as' => 'deleteFollowUserFollowerSettings',
    'middleware' => 'auth'
]);

Route::post('/saveInteractWithUserSettings/', [
        'uses' => 'InteractWithUserController@saveInteractWithUserSettings',
        'as' => 'saveInteractWithUserSettings',
    ]);
Route::get('/deleteInteractWithUserSettings/{ui_id}', [
    'uses' => 'InteractWithUserController@deleteInteractWithUserSettings',
    'as' => 'deleteInteractWithUserSettings',
    'middleware' => 'auth'
]);

//List routes
Route::post('/saveFollowUserFollowerList/', [
        'uses' => 'FollowUserFollowersController@saveFollowUserFollowerList',
        'as' => 'saveFollowUserFollowerList',
    ]);

Route::get('/deleteFollowUserFollowerList/{list_id}', [
    'uses' => 'FollowUserFollowersController@deleteFollowUserFollowerList',
    'as' => 'deleteFollowUserFollowerList',
    'middleware' => 'auth'
]);

Route::post('/saveCommentList/', [
        'uses' => 'InteractWithUserController@saveCommentList',
        'as' => 'saveCommentList',
    ]);

Route::get('/deleteCommentList/{ui_id}', [
    'uses' => 'InteractWithUserController@deleteCommentList',
    'as' => 'deleteCommentList',
    'middleware' => 'auth'
]);

Route::post('/saveLikeByTagList/', [
        'uses' => 'LikeByTagController@saveLikeByTagList',
        'as' => 'saveLikeByTagList',
    ]);
Route::get('/deleteLikeByTagList/{list_id}', [
    'uses' => 'LikeByTagController@deleteLikeByTagList',
    'as' => 'deleteLikeByTagList',
    'middleware' => 'auth'
]);
//END

//Save Tag
Route::post('/saveLike', [
    'uses' => 'SettingsController@saveLike',
    'as' => 'saveLike',
   
]);
Route::get('/deleteLike/{like_id}/{like_name}', [
    'uses' => 'SettingsController@deleteLike',
    'as' => 'deleteLike',
    'middleware' => 'auth'
]);
Route::post('/saveComment', [
    'uses' => 'InteractWithUserController@saveComment',
    'as' => 'saveComment',
   
]);
Route::get('/deleteComment/{list_id}/{comment_id}', [
    'uses' => 'InteractWithUserController@deleteComment',
    'as' => 'deleteComment',
    'middleware' => 'auth'
]);






Route::post('/saveFollowByTag', [
    'uses' => 'SettingsController@saveFollowByTag',
    'as' => 'saveFollowByTag',
   
]);
Route::get('/deleteFollowByTag/{FollowByTag_id}', [
    'uses' => 'SettingsController@deleteFollowByTag',
    'as' => 'deleteFollowByTag',
    'middleware' => 'auth'
]);

Route::post('/saveFollowUserFollowers', [
    'uses' => 'SettingsController@saveFollowUserFollowers',
    'as' => 'saveFollowUserFollowers',
   
]);
Route::get('/deleteFollowUserFollowers/{list_id}/{follow_name}', [
    'uses' => 'SettingsController@deleteFollowUserFollowers',
    'as' => 'deleteFollowUserFollowers',
    'middleware' => 'auth'
]);
Route::post('/saveLbt', [
    'uses' => 'SettingsController@saveLbt',
    'as' => 'saveLbt',
   
]);
