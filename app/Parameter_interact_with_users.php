<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter_interact_with_users extends Model
{
     public function user(){
		return $this->belongsTo('App\User');
    }
}
