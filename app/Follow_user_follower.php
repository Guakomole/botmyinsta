<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow_user_follower extends Model
{
    public function user(){
    	return $this->hasMany('App\User');
    }
}
