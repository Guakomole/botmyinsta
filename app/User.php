<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable;

class User extends Model implements Authenticatable
{
   protected $fillable = ['name', 'email', 'password'];
   use \Illuminate\Auth\Authenticatable;
    public function parameters(){
        return $this->hasMany('App\Parameter');
    }
    public function likes(){
        return $this->hasMany('App\Like');
    }
    public function follow_user_followers(){
        return $this->hasMany('App\Follow_user_followers');
    }
    public function smart_hashtag(){
        return $this->hasMany('App\Smart_hashtag');
    }
    public function comments(){
        return $this->hasMany('App\Comment');
    }
    public function user_interact(){
        return $this->belongsTo('App\User_interact');
    }
    public function relationship_bound(){
        return $this->belongsTo('App\Relationship_bounds');
    }
    public function like_by_tag(){
        return $this->hasMany('App\Like_by_tag');
    }
    public function parameter_follow_user_followers(){
        return $this->hasMany('App\Parameter_follow_user_followers');
    }
    public function parameter_like_by_tag(){
        return $this->hasMany('App\Parameter_like_by_tag');
    }
    public function parameter_comments(){
        return $this->hasMany('App\Parameter_comments');
    }
    public function parameter_follow_by_tags(){
        return $this->hasMany('App\Parameter_follow_by_tags');
    }
    public function like_by_tag_lists(){
        return $this->hasMany('App\Like_by_tag_lists');
    }
    public function follow_user_follower_lists(){
        return $this->hasMany('App\Follow_user_follower_lists');
    }
    public function comment_lists(){
        return $this->hasMany('App\Comment_lists');
    }
    public function interact_with_user(){
        return $this->hasMany('App\Interact_with_user');
    }
    public function interact_with_user_lists(){
        return $this->hasMany('App\interact_with_user_lists');
    }
    public function configured_user_bots(){
        return $this->hasMany('App\Configured_User_Bot');
    }
}
