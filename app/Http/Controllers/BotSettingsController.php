<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parameter;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BotSettingsController extends Controller
{
    public function index()
    { 
        $user = Auth::user();
        $userdata = User::where('id',$user->id)->first();
        $settings = DB::table('parameters')->where('user_id', Auth::id())->get();
       // dd($settings);
        if(sizeof($settings) < 1){
            $settings = new Parameter();
            $settings->user_id = $user->id;
            $settings->save();

        }
       //dd($likes);
        //$settings = $user->settings;
        // dd($settings);
        return view('botSettings', compact('userdata','settings'));
    }
}
