<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Parameter;
use App\User;
use App\Like_by_tag;
use App\Follow_user_follower;
use App\Follow_by_Tag;
use App\Comment;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class SettingsController extends Controller
{
    public function index()
    { 
        $user = Auth::user();
        $userdata = User::where('id',$user->id)->first();
        $settings = DB::table('parameters')->where('user_id', Auth::id())->get();
       // dd($settings);
        if(sizeof($settings) < 1){
            $settings = new Parameter();
            $settings->user_id = $user->id;
            $settings->save();

        }
       //dd($likes);
        //$settings = $user->settings;
        // dd($settings);
        return view('settings', compact('userdata','settings'));
    }
    public function saveLike(Request $request)
    {
        $this->validate($request, [
            'like' => 'required'
        ]);
        $user = Auth::user();
        $input = $request['like'];
        if (strpos($input, ',') == true) {
            $inputArray = explode(',', $input);
            for($z = 0; $z < sizeof($inputArray); $z++){
                if(strlen($inputArray[$z]) > 0){
                    $like = new Like_by_tag();
                    $like->user_id = $user->id;
                    $like->list_id = $request['lists'];
                    $like->tag_name = $inputArray[$z];
                    $like->save();
                }
            }
        }
        else{
            $like = new Like_by_tag();
            $like->user_id = $user->id;
            $like->list_id = $request['lists'];
            $like->tag_name = $input;

            $like->save();
        }

        return redirect()->route('settingsLikeByTags');
    }
    public function deleteLike($like_id, $like_name){
        $like = Like_by_tag::where('list_id', $like_id)->where('tag_name', $like_name)->first();
       // dd($like_id);
        /*if(Auth::user() != $like->user){
            return redirect()->route('settings');
        }*/
        $like->delete();
        return redirect()->route('settingsLikeByTags');
    }

    public function saveFollowByTag(Request $request)
    {
        $this->validate($request, [
            'follow_by_tag' => 'required'
        ]);
        $user = Auth::user();
        $exist = DB::table('follow_by_tags')->where('tag_name', $request['follow_by_tag'])->first();
        if (!$exist) {
            $follow_by_tag = new Follow_by_tag();
            $follow_by_tag->user_id = $user->id;
            $follow_by_tag->tag_name = $request['follow_by_tag'];

            $follow_by_tag->save();
        }
        return redirect()->route('settingsFollowByTags');
    }
    public function deleteFollowByTag($followByTag_id){
        $follow_by_tag = Follow_by_tag::where('id', $followByTag_id)->first();
       // dd($like_id);
        /*if(Auth::user() != $like->user){
            return redirect()->route('settings');
        }*/
        $follow_by_tag->delete();
        return redirect()->route('settingsFollowByTags');
    }

   
    public function saveFollowUserFollowers(Request $request)
    {
        $input = $request['follow'];
        $this->validate($request, [
            'follow' => 'required'
        ]);
        $user = Auth::user();

        if (strpos($input, ',') !== false) {
            $inputArray = explode(',', $input);
            for($z = 0; $z < sizeof($inputArray); $z++){
                if(strlen($inputArray[$z]) > 0){
                    $follow = new Follow_user_follower();
                    $follow->user_id = $user->id;
                    $follow->list_id = $request['lists'];
                    $follow->follow_name = $inputArray[$z];
                    $follow->save();
                }
            }
        }
        else{
            $follow = new Follow_user_follower();
            $follow->user_id = $user->id;
            $follow->list_id = $request['lists'];
            $follow->follow_name = $input;

            $follow->save();
        }
       // $exist = DB::table('follow_user_followers')->where('follow_name', $request['follow'])->first();
        //if (!$exist) {
            
        
        return redirect()->route('settingsFollowUserFollowers');
    }
     public function deleteFollowUserFollowers($list_id, $follow_name){
        $follow = Follow_user_follower::where('list_id', $list_id)->where('follow_name', $follow_name)->first();
       //dd($follow);
        /*if(Auth::user() != $like->user){
            return redirect()->route('settings');
        }*/
        $follow->delete();
        return redirect()->route('settingsFollowUserFollowers');
    }
    //public function addLike($request){}
    public function saveSettings(Request $request, $return){
    	$user = Auth::user();
        $settingsExist = DB::table('parameters')->where('user_id', Auth::id())->get();
    	$settings = new Parameter();
        $settings->user_id = $user->id;
    	$settings->instagram_username = $request['instagram_username'];
    	$settings->instagram_password = $request['instagram_password'];
    	$settings->rb_min_followers = $request['min_followers'];
    	$settings->rb_max_followers = $request['max_followers'];
    	$settings->rb_min_following = $request['min_following'];
    	$settings->rb_max_following = $request['max_following'];
    	$settings->rb_potency_ratio = 1.5;
    	$settings->rb_delimit_by_numbers = 11;
        if(sizeof($settingsExist) > 0){
            Parameter::where('user_id', $user->id)->update($request->except('_token','return'));
        }
        else{
            $settings->save();
        }
       
    	return redirect()->route($return);
    }
   
}
