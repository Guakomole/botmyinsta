<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Follow_by_Tag;
use App\Parameter_follow_by_tags;


class FollowByTagController extends Controller
{
	public function deleteFollowByTagSettings($set_id){
		$user = Auth::user();
		$follow = Parameter_follow_by_tags::where('user_id', $user->id)->where('set_id', $set_id)->first();
	    $follow->delete();
	    return redirect()->route('settingsFollowByTags');
	}
     public function saveFollowByTagSettings(Request $request){
    	$user = Auth::user();
        $follow = DB::table('Parameter_follow_by_tags')->where('user_id', $user->id)->orderBy('set_id', 'desc');  
       
        if($follow != null){
	        $set = $follow->count('set_id');
	        $set = $set + 1;
    	}
    	else{
    		$set = 1;
    	}
    	$follows = new Parameter_follow_by_tags();
        $follows->user_id = $user->id;
        $follows->set_id = $set;
    	$follows->amount = $request['amount'];
        $follows->save();      
    	return redirect()->route('settingsFollowByTags');
    }
}
