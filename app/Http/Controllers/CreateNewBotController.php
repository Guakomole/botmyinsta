<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parameter;
use App\User;
use App\Follow_user_follower;
use App\Configured_User_Bot;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreateNewBotController extends Controller
{
    public function saveNewBot(Request $request)
    {
        $user = Auth::user();
    	$configured_user_bots = DB::table('Configured_user_bots')->where('user_id', $user->id)->orderBy('bot_id', 'desc');  
       
        if($configured_user_bots != null){
	        $bot_id = $configured_user_bots->count('bot_id');
	        $bot_id = $bot_id + 1;
    	}
    	else{
    		$bot_id = 1;
    	}

        $botList = $request['BotList'];
		$keyword = $request['newBotKeywordTextbox'];
		$globalSettings = $request['newBotUseGlovalSettingsCheckbox'];
       	$bot = new Configured_user_bot();
       	$bot->user_id = $user->id;
       	$bot->bot_id = $bot_id;
       	$bot->keyword = $keyword;
       	$bot->mode = $botList;
       	$bot->which_parameter = $globalSettings;
       	$bot->save();
        return redirect()->route('home');
    }
    public function getSelectedBots(Request $request){
    	$user = Auth::user();
        $userdata = User::where('id',$user->id)->first();
        $configured_user_bots_raw = DB::table('configured_user_bots')->where('user_id', $user->id);

		$data = [];
        	     //   dd($request);

        if($request['getHashtagBots'] == 1){
        	$configured_user_bots_arr= DB::table('configured_user_bots')->where('user_id', $user->id)->where('mode', '0')->get()->all();
        	$data = array_merge($data,$configured_user_bots_arr);
        }
        if($request['getKanalBots'] == 1){
        	$configured_user_bots_arr= DB::table('configured_user_bots')->where('user_id', $user->id)->where('mode', '1')->get()->all();
			$data = array_merge($data,$configured_user_bots_arr);
        }
        if($request['getNutzerBots'] == 1){
        	$configured_user_bots_arr= DB::table('configured_user_bots')->where('user_id', $user->id)->where('mode', '2')->get()->all();
        	$data = array_merge($data,$configured_user_bots_arr);
        }
        $configured_user_bots = $data;
    	return view('home', compact('userdata','configured_user_bots'));
    }
}
