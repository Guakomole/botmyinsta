<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Like_by_tag;
use App\Follow_user_follower;
use Illuminate\Support\Facades\DB;
use App\Parameter;
use App\BotUsingLog;
use Illuminate\Support\Collection;
use App\Like_by_tag_lists;

class FollowList{
    public $list_id;
    public $listItems;
    public $listname;
    public function __construct()
    {
        $this->listItems =  new Collection();
        $this->listname = $this->listname;
    }
    public function addItem($item){
        $this->listItems->push($item);
    }
}    
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSettingsLikeByTags(){
        $listContainers = new Collection();
        $likeLists = DB::table('like_by_tag_lists')->where('user_id', Auth::id())->get();
        $parameter_like_by_tags = DB::table('parameter_like_by_tags')->where('user_id', Auth::id())->get();
        $likes = DB::table('like_by_tags')->where('user_id', Auth::id())->get();
        foreach($likeLists as $likeList){
            $llist = new FollowList();
            $likeListsItems = DB::table('like_by_tags')->where('user_id', Auth::id())->where('list_id', $likeList->list_id)->get();
            $likes = DB::table('like_by_tags')->where('user_id', Auth::id())->where('list_id',  $likeList->list_id)->get();
            $llist->list_id = $likeList->list_id;
            $llist->listname = $likeList->list_name;
            foreach($likes as $like){
                $llist->addItem($like->tag_name);
            }
            $listContainers->push($llist);
        }
        return view('settings_like_by_tags', compact('likes','parameter_like_by_tags', 'likeLists', 'listContainers'));
    }
    public function getSettingsFollowUserFollowers(){
        $listContainers = new Collection();
        $parameter_follow_user_followers = DB::table('parameter_follow_user_followers')->where('user_id', Auth::id())->get();
        $followLists = DB::table('follow_user_follower_lists')->where('user_id', Auth::id())->get();
        foreach($followLists as $followList){
            $flist = new FollowList();
            //$followListsItems = DB::table('follow_user_followers')->where('user_id', Auth::id())->where('list_id', $followList->list_id)->get();
            $follows = DB::table('follow_user_followers')->where('user_id', Auth::id())->where('list_id',  $followList->list_id)->get();
            $flist->list_id = $followList->list_id;
            $flist->listname = $followList->list_name;
            foreach($follows as $follow){
                $flist->addItem($follow->follow_name);
            }
            $listContainers->push($flist);
        }
        //dd($listContainers);
        return view('settings_follow_user_followers', compact('follows','parameter_follow_user_followers', 'followLists', 'listContainers'));
    }
    public function getSettingsCommentTags(){
        $listContainers = new Collection();
        $parameter_interact_with_users = DB::table('parameter_interact_with_users')->where('user_id', Auth::id())->get();
        $interactWithUserLists = DB::table('comment_lists')->where('user_id', Auth::id())->get();
        foreach($interactWithUserLists as $interactWithUserList){
            $flist = new FollowList();
            $interactWithUserListsItems = DB::table('comments')->where('user_id', Auth::id())->where('list_id',  $interactWithUserList->list_id)->get();
            $flist->list_id = $interactWithUserList->list_id;
            $flist->listname = $interactWithUserList->list_name;
            foreach($interactWithUserListsItems as $interactWithUserListsItem){
                $flist->addItem($interactWithUserListsItem->comment);
            }
            $listContainers->push($flist);
        }

        return view('settings_comments', compact('comments', 'parameter_interact_with_users', 'interactWithUserLists', 'parameter_interact_with_users', 'listContainers'));
    }
    public function getSettingsFollowByTags(){
        $follow_by_tags = DB::table('follow_by_tags')->where('user_id', Auth::id())->get();
        $parameter_follow_by_tags = DB::table('parameter_follow_by_tags')->where('user_id', Auth::id())->get();

        return view('settings_follow_by_tag', compact('follow_by_tags','parameter_follow_by_tags'));
    }
    public function saveCheckedBot($request, $botname, $botI, $bot_cat, $process_id){
        for($i = 0; $i < $botI; $i++){
            if($request[$botname."_bot_".$i] != 0){
                $user = Auth::user();
                $botUsingLog = new botUsingLog();
                $botUsingLog->user_id = $user->id;
                $botUsingLog->process_id = $process_id;
                $botUsingLog->bot_category_id = $bot_cat;
                $botUsingLog->set_id = $request[$botname."_bot_".$i];
                $botUsingLog->list_id = $request["lists_".$botname."_".$i];
                $botUsingLog->save();   
            }   
        }
    }
    public function getDashboard()
    {

        $user = Auth::user();
        $userdata = User::where('id',$user->id)->first();
        $configured_user_bots = DB::table('configured_user_bots')->where('user_id', Auth::id())->get();

        /*
        $parameter_comments = DB::table('parameter_comments')->where('user_id', Auth::id())->get();
        $parameter_like_by_tags = DB::table('parameter_like_by_tags')->where('user_id', Auth::id())->get();
        $parameter_follow_user_followers = DB::table('parameter_follow_user_followers')->where('user_id', Auth::id())->get();
        $parameter_interact_with_users = DB::table('parameter_interact_with_users')->where('user_id', Auth::id())->get();

        $likeLists = DB::table('like_by_tag_lists')->where('user_id', Auth::id())->get();
        $followLists = DB::table('follow_user_follower_lists')->where('user_id', Auth::id())->get();
        $commentLists = DB::table('comment_lists')->where('user_id', Auth::id())->get();
        $likes = DB::table('like_by_tags')->where('user_id', Auth::id())->get();
        $follows = DB::table('follow_user_followers')->where('user_id', Auth::id())->get();
        $comments = DB::table('comments')->where('user_id', Auth::id())->get();
        */
        return view('home', compact('userdata','configured_user_bots'/*, 'likes', 'follows', 'comments', 'parameter_like_by_tags', 'parameter_follow_user_followers', 'parameter_comments','parameter_interact_with_users', 'followLists', 'likeLists', 'commentLists'*/));
    }
     public function runBot(Request $request){
        //dd($request);
        $user = Auth::user();
        $parameter_like_by_tags = DB::table('parameter_like_by_tags')->where('user_id', Auth::id())->get();
        $parameter_interact_with_users = DB::table('parameter_interact_with_users')->where('user_id', Auth::id())->get();
        $parameter_follow_user_followers = DB::table('parameter_follow_user_followers')->where('user_id', Auth::id())->get();

        $botUsingLog = DB::table('bot_using_logs')->where('user_id', $user->id)->orderBy('process_id', 'desc');
        if($botUsingLog->first() != null){
            $processNumber = $botUsingLog->first()->process_id;
            $processNumber = $processNumber + 1;
        }
        else{
            $processNumber = 1;
        }

        $numberLikeBots = $parameter_like_by_tags->count('set_id)');
        $numberInteractWithUserBots = $parameter_interact_with_users->count('set_id)');
        $numberFollowUserFollowerBots = $parameter_follow_user_followers->count('set_id)');
        
        $this->saveCheckedBot($request,'lbt', $numberLikeBots, 1 , $processNumber);
        $this->saveCheckedBot($request,'fuf', $numberFollowUserFollowerBots, 2, $processNumber);
        $this->saveCheckedBot($request,'ui', $numberInteractWithUserBots, 3, $processNumber);
        
        //dd($request);
        $output = exec ('sudo python /home/mar/InstaPy/instabot.py '.$user->id.' > /dev/null 2>&1 &');
        /*
        $process = new Process("sudo python /home/mar/InstaPy/quickstart.py > /dev/null 2>&1 &");
        $process->run();
       // executes after the command finishes  
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        */
        return redirect()->route('home');
   }
   
}
