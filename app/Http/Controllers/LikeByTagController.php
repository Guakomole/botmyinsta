<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Parameter_like_by_tag;
use App\Like_by_tag_lists;

class LikeByTagController extends Controller
{
    public function deleteLikeByTagSettings($set_id){
		$user = Auth::user();
		$like = Parameter_like_by_tag::where('user_id', $user->id)->where('set_id', $set_id)->first();
	    $like->delete();
	    return redirect()->route('settingsLikeByTags');
	}
     public function saveLikeByTagSettings(Request $request){
    	$user = Auth::user();
        $like = DB::table('parameter_like_by_tags')->where('user_id', $user->id)->orderBy('set_id', 'desc');  
       
        if($like != null){
	        $set = $like->count('set_id');
	        $set = $set + 1;
    	}
    	else{
    		$set = 1;
    	}
       	$like = new Parameter_like_by_tag();
        $like->user_id = $user->id;
        $like->set_id = $set;
    	$like->amount = $request['amount'];
    	$like->randomize = $request['randomize'];
    	$like->interact = $request['interact'];
    	$like->percentage = $request['percentage'];
    	$like->media = 0;
    	$like->use_smart_hashtags = $request['use_smart_hashtags'];;
    	$like->ui_amount = 1;
    	$like->ui_randomize = 0;
    	$like->ui_percentage = 20;
    	$like->ui_media = 1;
        $like->save();      
    	return redirect()->route('settingsLikeByTags');
    }

    public function saveLikeByTagList(Request $request){
        $user = Auth::user();

        $likeLists = DB::table('like_by_tag_lists')->where('user_id', $user->id)->orderBy('list_id', 'desc')->first();  
        if($likeLists != null){
            $list = $likeLists->list_id;
            $list = $list + 1;
        }
        else{
            $list = 1;
        }

        $likeList = new Like_by_tag_lists();
        $likeList->user_id = $user->id;
        $likeList->list_id = $list;
        $likeList->list_name = $request['list_name'];
        $likeList->save();
        return redirect()->route('settingsLikeByTags');
    }
    public function deleteLikeByTagList($set_id){
        $user = Auth::user();
        $like = Like_by_tag_lists::where('user_id', $user->id)->where('list_id', $set_id)->first();
        $likesFromList = Like_by_tags::where('user_id', $user->id)->where('list_id', $set_id)->get();
        foreach($likesFromList as $item){
            $item->delete();
        }
        $like->delete();

        return redirect()->route('settingsLikeByTags');
    }
}
