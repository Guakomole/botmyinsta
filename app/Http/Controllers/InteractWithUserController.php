<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Interact_with_user_lists;
use App\Parameter_interact_with_users;
use App\Comment_lists;
use App\Comment;
use Illuminate\Support\Facades\DB;

class InteractWithUserController extends Controller
{

	public function saveInteractWithUserSettings(Request $request){
    	$user = Auth::user();
        $user_interact = DB::table('parameter_interact_with_users')->where('user_id', $user->id)->orderBy('set_id', 'desc');  
       
        if($user_interact != null){
	        $set = $user_interact->count('set_id');
	        $set = $set + 1;
    	}
    	else{
    		$set = 1;
    	}

    	$user_interacts = new Parameter_interact_with_users();
        $user_interacts->user_id = $user->id;
        $user_interacts->set_id = $set;
    	$user_interacts->ui_amount = $request['ui_amount'];
    	$user_interacts->ui_randomize = $request['ui_randomize'];
    	$user_interacts->ui_percentage = $request['ui_percentage'];
    	$user_interacts->dofollow_enabled = $request['dofollow_enabled'];
    	$user_interacts->dofollow_percentage = $request['dofollow_percentage'];
    	$user_interacts->dolike_enabled = $request['dolike_enabled'];
    	$user_interacts->dolike_percentage = $request['dolike_percentage'];
    	$user_interacts->docomment_enabled = $request['docomment_enabled'];
    	$user_interacts->docomment_percentage = $request['docomment_percentage'];
        $user_interacts->save();  
           
    	return redirect()->route('settingsCommentTags');
    }
    public function deleteInteractWithUserSettings($set_id){
		$user = Auth::user();
		$user_interact = Parameter_interact_with_users::where('user_id', $user->id)->where('set_id', $set_id)->first();
	    $user_interact->delete();
	    return redirect()->route('settingsCommentTags');
    }

/*
     public function saveInteractWithUser(Request $request){
        $input = $request['user_interact'];
        $this->validate($request, [
            'user_interact' => 'required'
        ]);
        $user = Auth::user();

        if (strpos($input, ',') !== false) {
            $inputArray = explode(',', $input);
            for($z = 0; $z < sizeof($inputArray); $z++){
                if(strlen($inputArray[$z]) > 0){
                    $user_interact = new InteractWithUser();
                    $user_interact->user_id = $user->id;
                    $user_interact->list_id = $request['lists'];
                    $user_interact->user_interact_name = $inputArray[$z];
                    $user_interact->save();
                }
            }
        }
        else{
            $user_interact = new InteractWithUser();
            $user_interact->user_id = $user->id;
            $user_interact->list_id = $request['lists'];
            $user_interact->user_interact_name = $input;

            $user_interact->save();
        }
        return redirect()->route('settingsCommentTags');
    }

     public function deleteInteractWithUser($ui_id){
        $ui = Interact_with_user::where('id', $ui_id)->first();
       // dd($like_id);
    
        
        $ui->delete();
        return redirect()->route('settingsCommentTags');
    }
    */

    public function saveCommentList(Request $request){
        $user = Auth::user();

        $interactWithUserLists = DB::table('comment_lists')->where('user_id', $user->id)->orderBy('list_id', 'desc')->first();  
        if($interactWithUserLists != null){
            $list = $interactWithUserLists->list_id;
            $list = $list + 1;
        }
        else{
            $list = 1;
        }

        $interactWithUserLists = new Comment_lists();
        $interactWithUserLists->user_id = $user->id;
        $interactWithUserLists->list_id = $list;
        $interactWithUserLists->list_name = $request['list_name'];
        $interactWithUserLists->save();
        return redirect()->route('settingsCommentTags');
    }
    public function deleteCommentList($set_id){
        $user = Auth::user();
        $comment = Comment_lists::where('user_id', $user->id)->where('list_id', $set_id)->first();
        $commentsFromList = Comment::where('user_id', $user->id)->where('list_id', $set_id)->get();
        foreach($commentsFromList as $item){
            $item->delete();
        }
        $comment->delete();
        return redirect()->route('settingsCommentTags');
    }

     public function saveComment(Request $request){
        $this->validate($request, [
            'comment' => 'required'
        ]);
        $user = Auth::user();
        $input = $request['comment'];
        if (strpos($input, ',') == true) {
            $inputArray = explode(',', $input);
            for($z = 0; $z < sizeof($inputArray); $z++){
                if(strlen($inputArray[$z]) > 0){
                    $comment = new Comment();
                    $comment->user_id = $user->id;
                    $comment->list_id = $request['lists'];
                    $comment->comment = $inputArray[$z];
                    $comment->save();
                }
            }
        }
        else{
            $comment = new Comment();
            $comment->user_id = $user->id;
            $comment->list_id = $request['lists'];
            $comment->comment = $input;
            $comment->save();
        }
        return redirect()->route('settingsCommentTags');
    }

    public function deleteComment($list_id, $comment){
        $comment = Comment::where('list_id', $list_id)->where('comment', $comment)->first();

       // dd($like_id);
        /*if(Auth::user() != $like->user){
            return redirect()->route('settings');
        }*/
        $comment->delete();
        return redirect()->route('settingsCommentTags');
    }
    public function deleteCommentSettings($set_id){
        $user = Auth::user();
        $comment = Parameter_comments::where('user_id', $user->id)->where('set_id', $set_id)->first();
        $comment->delete();
        return redirect()->route('settingsCommentTags');
    }
     public function saveCommentSettings(Request $request){
        $user = Auth::user();
        $comment = DB::table('parameter_comments')->where('user_id', $user->id)->orderBy('set_id', 'desc');  
       
        if($comment != null){
            $set = $comment->count('set_id');
            $set = $set + 1;
        }
        else{
            $set = 1;
        }
        $comments = new Parameter_comments();
        $comments->user_id = $user->id;
        $comments->set_id = $set;
        $comments->enabled = $request['enabled'];
        $comments->percentage = $request['percentage'];
        $comments->save();      
        return redirect()->route('settingsCommentTags');
    }
}
