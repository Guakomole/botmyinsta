<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Parameter_follow_user_followers;
use App\Follow_user_follower_lists;
use App\Follow_user_follower;
use Illuminate\Support\Facades\DB;

class FollowUserFollowersController extends Controller
{
	public function saveFollowUserFollowerSettings(Request $request){
    	$user = Auth::user();
        $follow = DB::table('parameter_follow_user_followers')->where('user_id', $user->id)->orderBy('set_id', 'desc');  
       
        if($follow != null){
	        $set = $follow->count('set_id');
	        $set = $set + 1;
    	}
    	else{
    		$set = 1;
    	}
    	$follows = new Parameter_follow_user_followers();
        $follows->user_id = $user->id;
        $follows->set_id = $set;
    	$follows->amount = $request['amount'];
    	$follows->sleep_delay = $request['sleep_delay'];
    	$follows->randomize = $request['randomize'];
        $follows->save();  
           
    	return redirect()->route('settingsFollowUserFollowers');
    }
    public function deleteFollowUserFollowerSettings($set_id){
		$user = Auth::user();
		$follow = Parameter_follow_user_followers::where('user_id', $user->id)->where('set_id', $set_id)->first();
	    $follow->delete();
	    return redirect()->route('settingsFollowUserFollowers');
    }
    public function saveFollowUserFollowerList(Request $request){
        $user = Auth::user();

        $followLists = DB::table('follow_user_follower_lists')->where('user_id', $user->id)->orderBy('list_id', 'desc')->first();  
        if($followLists != null){
            $list = $followLists->list_id;
            $list = $list + 1;
        }
        else{
            $list = 1;
        }

        $followList = new Follow_user_follower_lists();
        $followList->user_id = $user->id;
        $followList->list_id = $list;
        $followList->list_name = $request['list_name'];
        $followList->save();
        return redirect()->route('settingsFollowUserFollowers');
    }
    public function deleteFollowUserFollowerList($set_id){
        $user = Auth::user();
        $follow = Follow_user_follower_lists::where('user_id', $user->id)->where('list_id', $set_id)->first();
        $followsFromList = Follow_user_follower::where('user_id', $user->id)->where('list_id', $set_id)->get();
        foreach($followsFromList as $item){
            $item->delete();
        }
        $follow->delete();

        return redirect()->route('settingsFollowUserFollowers');
    }
}
