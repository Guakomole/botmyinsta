<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow_user_follower_lists extends Model
{
    public function user(){
		return $this->belongsTo('App\User');
    }
}
