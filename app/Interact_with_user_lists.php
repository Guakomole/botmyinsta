<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interact_with_user_lists extends Model
{
     public function user(){
		return $this->belongsTo('App\User');
    }
}
