<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter_follow_user_followers extends Model
{
    public function user(){
		return $this->belongsTo('App\User');
    }
}
