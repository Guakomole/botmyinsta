<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    public function user(){
    	return $this->belongsTo('App\User');
    }
}
